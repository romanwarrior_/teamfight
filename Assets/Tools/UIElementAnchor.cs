using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIElementAnchor : MonoBehaviour
{
    [SerializeField] private Transform target;
    [SerializeField] private Vector3 offset;

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    private void Update()
    {
        if (target)
        {
            transform.position = Camera.main.WorldToScreenPoint(target.position + offset); 
        }
    }
}
