using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GlobalEvents : MonoBehaviour
{
    public UnityEvent<Character> OnCharacterDied;
    public UnityEvent<int> OnTeamWon;
    public UnityEvent<List<Character>> OnBotsSpawned;
    public UnityEvent OnGameRestarted;

    #region Singleton
    private static GlobalEvents instance;

    public static GlobalEvents Instance
    {
        get
        {
            if (instance == null) instance = FindObjectOfType<GlobalEvents>();
            return instance;
        }
    }
    #endregion
}
