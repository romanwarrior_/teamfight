using System.Collections.Generic;
using UnityEngine;
using UnityNightPool;

public class BotsSpawner : MonoBehaviour
{
    [SerializeField] private Transform[] teamSpawnpoint;
    [SerializeField] private float spawnRange;

    private List<Character> bots = new List<Character>();

    private void Start()
    {
        GlobalEvents.Instance.OnGameRestarted.AddListener(OnGameStarted);
        GlobalEvents.Instance.OnCharacterDied.AddListener(OnCharacterDied);
    }

    private void SpawnBots(int amount)
    {
        bots = new List<Character>();
        for (int i = 0; i < amount; i++)
        {
            Character bot = PoolManager.Get(1).GetComponent<Character>();

            bot.SetTeam(i / (amount / 2));
            Vector3 spawnpoint = teamSpawnpoint[i / (amount / 2)].position + Random.insideUnitSphere * spawnRange;
            spawnpoint = new Vector3(spawnpoint.x, 0, spawnpoint.z);
            bot.transform.position = spawnpoint;
            bots.Add(bot);
        }
        foreach (var bot in bots)
        {
            bot.Init(bots);
        }
        GlobalEvents.Instance.OnBotsSpawned?.Invoke(bots);
    }

    private void OnCharacterDied(Character character)
    {
        bots.Remove(character);
    }

    private void OnGameStarted()
    {
        Character[] botsLeft = bots.ToArray();

        foreach (var bot in botsLeft)
        {
            Destroy(bot.gameObject);
        }
        SpawnBots(6);
    }
}
