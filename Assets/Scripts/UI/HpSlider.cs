using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
[RequireComponent(typeof(UIElementAnchor))]

public class HpSlider : MonoBehaviour
{
    private Character character;
    private Slider slider;

    public void SetTarget(Character target)
    {
        character = target;
        GetComponent<UIElementAnchor>().SetTarget(target.transform);
        target.OnDamageTaken.AddListener(UpdateUI);
    }

    private void Awake()
    {
        slider = GetComponent<Slider>();
    }

    private void OnEnable()
    {
        slider.maxValue = character.MaxHp;
        slider.value = character.Hp;
    }

    public void UpdateUI()
    {
        slider.value = character.Hp;
        if (character==null || character.Hp <= 0) gameObject.SetActive(false);
    }
}
