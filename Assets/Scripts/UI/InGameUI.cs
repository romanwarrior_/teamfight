using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InGameUI : MonoBehaviour
{
    [SerializeField] private GameObject gameOverWindow;
    [SerializeField] private TextMeshProUGUI victoryText;
    [SerializeField] private TextMeshProUGUI timeText;
    [SerializeField] private HpSlider[] hpSliders;

    private void Awake()
    {
        GlobalEvents.Instance.OnTeamWon.AddListener(ShowGameOverWindow);
        GlobalEvents.Instance.OnBotsSpawned.AddListener(InitSliders);
    }

    private void ShowGameOverWindow(int teamWon)
    {
        gameOverWindow.SetActive(true);
        if (teamWon == 0)
        {
            victoryText.text = "Blue team won";
            victoryText.color = new Color32(0, 0, 187, 255);
        }
        else
        {
            victoryText.text = "Red team won";
            victoryText.color = new Color32(187, 0, 0, 255);
        }
        timeText.text = $"Battle lasted {Math.Round(TimeCounter.TimeSpent,1)} sec.";
    }

    private void InitSliders(List<Character> bots)
    {
        for(int i = 0; i < bots.Count; i++)
        {
            hpSliders[i].SetTarget(bots[i]);
            hpSliders[i].gameObject.SetActive(true);
            hpSliders[i].UpdateUI();
        }
    }
}
