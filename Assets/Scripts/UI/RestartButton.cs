using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartButton : MonoBehaviour
{
    public void Click()
    {
        transform.parent.gameObject.SetActive(false);
        GlobalEvents.Instance.OnGameRestarted?.Invoke();
    }
}
