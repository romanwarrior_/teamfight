using UnityEngine;

public class TimeCounter : MonoBehaviour
{
    public static float TimeSpent => time;

    private static float time;
    private bool isCounting;

    private void Awake()
    {
        GlobalEvents.Instance.OnGameRestarted.AddListener(StartCounting);
        GlobalEvents.Instance.OnTeamWon.AddListener(StopCounting);
    }

    private void Start()
    {
        StartCounting();
    }

    private void Update()
    {
        if (isCounting) time += Time.deltaTime;
    }

    private void StartCounting()
    {
        time = 0;
        isCounting = true;
    }

    private void StopCounting(int _ = 0)
    {
        isCounting = false;
    }
}
