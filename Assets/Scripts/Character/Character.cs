using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Character : MonoBehaviour
{
    [SerializeField] private MeshRenderer mesh;
    [SerializeField] private Material[] colors;

    [Header("Characteristics")]
    [SerializeField] private float maxHp;
    [SerializeField] private float speed;
    [SerializeField] private float attackRange;
    [SerializeField] private float damage;
    [SerializeField] private float attackRate;

    public UnityEvent OnDamageTaken; 

    public int Team => team;
    public float AttackRange => attackRange;
    public float AttackRate => attackRate;
    public float Speed => speed;
    public float Damage => damage;
    public float Hp => hp;
    public float MaxHp => maxHp;

    private List<Character> enemiesAlive = new List<Character>();
    private int team;
    private float hp;

    public void Init(List<Character> characters)
    {
        foreach (var character in characters)
        {
            if (character.Team != team)
            {
                enemiesAlive.Add(character);
            }
        }

        hp = maxHp;
        GlobalEvents.Instance.OnCharacterDied.AddListener(OnCharacterDied);
    }

    public void GetDamage(float value)
    {
        hp -= value;
        OnDamageTaken?.Invoke();
        if (hp <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        GlobalEvents.Instance.OnCharacterDied?.Invoke(this);
        Destroy(gameObject);
    }

    public Character SearchForTarget()
    {
        float closestDistance = Mathf.Infinity;
        Character closestCharacter = null;
        foreach (var character in enemiesAlive)
        {
            if (character.team == team)
                continue;

            float distance = Vector3.Distance(transform.position, character.transform.position);
            if (distance < closestDistance)
            {
                closestDistance = distance;
                closestCharacter = character;
            }
        }
        return closestCharacter;
    }

    private void OnCharacterDied(Character character)
    {
        if (character.Team != team)
        {
            enemiesAlive.Remove(character);
            if (enemiesAlive.Count == 0) GlobalEvents.Instance.OnTeamWon?.Invoke(team);
        }
    }

    public void SetTeam(int team)
    {
        this.team = team;
        mesh.material = colors[team];
    }
}
