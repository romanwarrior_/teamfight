using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class CharacterAttackComponent : MonoBehaviour
{
    [SerializeField] private Character character;

    public void StartAttacking(Character target)
    {
        Observable.Interval(new TimeSpan(0, 0, 0, 0, Convert.ToInt32(1000 / character.AttackRate)))
                  .TakeUntilDisable(target.gameObject)
                  .TakeUntilDisable(gameObject)
                  .Subscribe(_=> 
        {
            target.GetDamage(character.Damage);
        });

        Vector3 direction = target.transform.position - transform.position;
        Vector3 requiredRotation = Quaternion.LookRotation(direction).eulerAngles;

        transform.DORotate(requiredRotation,0.3f);
    }
}
