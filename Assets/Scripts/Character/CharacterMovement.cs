using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private Character character;

    private const float RotationSpeed = 3;
    private Character currentMoveTarget;

    private void Start()
    {
        GlobalEvents.Instance.OnCharacterDied.AddListener(OnCharacterDied);
        currentMoveTarget = character.SearchForTarget();
    }

    private void Update()
    {
        Move();
    }

    private void Move()
    {
        if (!currentMoveTarget) return;

        float distance = Vector3.Distance(transform.position, currentMoveTarget.transform.position);
        if (distance > character.AttackRange)
        {
            transform.position = Vector3.Lerp(transform.position, currentMoveTarget.transform.position, Time.deltaTime*character.Speed);

            Vector3 requiredRotation = Quaternion.LookRotation(currentMoveTarget.transform.position - transform.position).eulerAngles;
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, requiredRotation, Time.deltaTime* RotationSpeed);
        }
    }

    private void OnCharacterDied(Character character)
    {
        currentMoveTarget = this.character.SearchForTarget();
    }

}
