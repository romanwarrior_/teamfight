using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetector : MonoBehaviour
{
    [SerializeField] private Character character;
    [SerializeField] private CharacterAttackComponent attack;

    private Character currentTarget;

    private void OnTriggerStay(Collider other)
    {
        if (currentTarget) return;

        if(other.TryGetComponent(out Character character))
        {
            if (this.character.Team != character.Team)
            {
                currentTarget = character;
                attack.StartAttacking(currentTarget);
            }
        }
    }

}
